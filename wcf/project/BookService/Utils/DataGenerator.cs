﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookService.Models;

namespace BookService.Utils
{
    public static class DataGenerator
    {
        public static List<Book> GenerateBooks(int quantity)
        {
            var books = new List<Book>();

            for (int i = 0; i < quantity; i++)
            {
                books.Add(new Book
                {
                    BookId = i + 1,
                    BookInfo = new BookInfo
                    {
                        Title = "Title " + (i + 1),
                        Author = "Author" + (i + 1),
                        PublicationDate = 2000 + (i + 1)
                    },
                    ReturnDate = DateTime.Now,
                    Status = new Status
                    {
                        IsBorrowed = false,
                        ReturnDate = DateTime.Now
                    }
                });
            }

            return books;

        }

        public static List<User> GenerateUsers(int quantity)
        {
            var users = new List<User>();

            for (int i = 0; i < quantity; i++)
            {
                users.Add(new User
                {
                    UserId = i + 1,
                    Name = "User " + (i + 1),
                    Surname = "User " + (i + 1)
                });
            }

            return users;

        }
    }
}
