﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BookService.Utils
{   [DataContract]
    public class UserNotFoundException
    {
        [DataMember]
        public string Message;
    }
}
