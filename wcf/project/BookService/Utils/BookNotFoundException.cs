﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BookService.Utils
{
    [DataContract]
    public class BookNotFoundException
    {
        [DataMember]
        public string Message;

        //public BookNotFoundException(string message) 
        //{

        //}
    }
}
