﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BookService.Models;
using BookService.Utils;

namespace BookService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        List<Book> ListOfBorrowedItems();

        [OperationContract]
        List<Book> GetBorrowedBooks(int userId);

        [OperationContract]
        [FaultContract(typeof(BookNotFoundException))]
        BookInfo GetBookInfo(int bookId);

        [OperationContract]
        [FaultContract(typeof(BookNotFoundException))]
        [FaultContract(typeof(UserNotFoundException))]
        Status BorrowBook(int bookId, int userId);

    }
}
