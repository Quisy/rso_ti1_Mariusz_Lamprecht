﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookService.Models
{
    public class BookInfo
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public int PublicationDate { get; set; }
    }
}
