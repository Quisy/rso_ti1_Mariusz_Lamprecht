﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookService.Models
{
    public class Status
    {
        public bool IsBorrowed { get; set; }
        public DateTime ReturnDate { get; set; }
    }
}
