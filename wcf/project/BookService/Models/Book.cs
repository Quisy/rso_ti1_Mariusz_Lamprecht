﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookService.Models
{
    public class Book
    {
        public int BookId { get; set; }
        public BookInfo BookInfo { get; set; }
        public DateTime ReturnDate { get; set; }
        public Status Status;
    }
}
