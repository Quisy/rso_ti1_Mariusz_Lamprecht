﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BookService.Models;
using BookService.Utils;

namespace BookService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Service1 : IService1
    {
        private static List<Book> _books;
        private static List<User> _users;

        public Service1()
        {
            if (_books == null)
                _books = DataGenerator.GenerateBooks(10);

            if (_users == null)
                _users = DataGenerator.GenerateUsers(1);
        }

        public List<Book> ListOfBorrowedItems()
        {
            return _books.Where(b => b.Status.IsBorrowed).ToList();
        }

        public List<Book> GetBorrowedBooks(int userId)
        {
            var user = _users.Find(u => u.UserId == userId);

            if (user == null)
            {
                throw new FaultException<UserNotFoundException>(new UserNotFoundException() { Message = "User not found" });
            }
            return user.BorrowedBooks;
        }

        public BookInfo GetBookInfo(int bookId)
        {
            var book = _books.Find(b => b.BookId == bookId);

            if (book == null)
            {
                throw new FaultException<BookNotFoundException>(new BookNotFoundException() {Message = "Book not found"});
            }

            return book.BookInfo;
        }

        public Status BorrowBook(int bookId, int userId)
        {
            var book = _books.Find(b => b.BookId == bookId);
            var user = _users.Find(u => u.UserId == userId);

            if (book == null)
            {
                //throw new BookNotFoundException("Book not found");
                throw new FaultException<BookNotFoundException>(new BookNotFoundException() { Message = "Book not found" });
            }

            if (user == null)
            {
                throw new FaultException<UserNotFoundException>(new UserNotFoundException() { Message = "User not found" });
            }

            if (book.Status.IsBorrowed || user.BorrowedBooks.Contains(book))
                return book.Status;

            book.Status.IsBorrowed = true;
            book.Status.ReturnDate += new TimeSpan(20, 0, 0, 0);
            book.ReturnDate += new TimeSpan(20, 0, 0, 0);

            user.BorrowedBooks.Add(book);

            return book.Status;
        }

    }
}
