﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BookClient.BookService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Book", Namespace="http://schemas.datacontract.org/2004/07/BookService.Models")]
    [System.SerializableAttribute()]
    public partial class Book : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int BookIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private BookClient.BookService.BookInfo BookInfoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime ReturnDateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private BookClient.BookService.Status StatusField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int BookId {
            get {
                return this.BookIdField;
            }
            set {
                if ((this.BookIdField.Equals(value) != true)) {
                    this.BookIdField = value;
                    this.RaisePropertyChanged("BookId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public BookClient.BookService.BookInfo BookInfo {
            get {
                return this.BookInfoField;
            }
            set {
                if ((object.ReferenceEquals(this.BookInfoField, value) != true)) {
                    this.BookInfoField = value;
                    this.RaisePropertyChanged("BookInfo");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime ReturnDate {
            get {
                return this.ReturnDateField;
            }
            set {
                if ((this.ReturnDateField.Equals(value) != true)) {
                    this.ReturnDateField = value;
                    this.RaisePropertyChanged("ReturnDate");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public BookClient.BookService.Status Status {
            get {
                return this.StatusField;
            }
            set {
                if ((object.ReferenceEquals(this.StatusField, value) != true)) {
                    this.StatusField = value;
                    this.RaisePropertyChanged("Status");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="BookInfo", Namespace="http://schemas.datacontract.org/2004/07/BookService.Models")]
    [System.SerializableAttribute()]
    public partial class BookInfo : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string AuthorField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int PublicationDateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string TitleField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Author {
            get {
                return this.AuthorField;
            }
            set {
                if ((object.ReferenceEquals(this.AuthorField, value) != true)) {
                    this.AuthorField = value;
                    this.RaisePropertyChanged("Author");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int PublicationDate {
            get {
                return this.PublicationDateField;
            }
            set {
                if ((this.PublicationDateField.Equals(value) != true)) {
                    this.PublicationDateField = value;
                    this.RaisePropertyChanged("PublicationDate");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Title {
            get {
                return this.TitleField;
            }
            set {
                if ((object.ReferenceEquals(this.TitleField, value) != true)) {
                    this.TitleField = value;
                    this.RaisePropertyChanged("Title");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Status", Namespace="http://schemas.datacontract.org/2004/07/BookService.Models")]
    [System.SerializableAttribute()]
    public partial class Status : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool IsBorrowedField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime ReturnDateField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool IsBorrowed {
            get {
                return this.IsBorrowedField;
            }
            set {
                if ((this.IsBorrowedField.Equals(value) != true)) {
                    this.IsBorrowedField = value;
                    this.RaisePropertyChanged("IsBorrowed");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime ReturnDate {
            get {
                return this.ReturnDateField;
            }
            set {
                if ((this.ReturnDateField.Equals(value) != true)) {
                    this.ReturnDateField = value;
                    this.RaisePropertyChanged("ReturnDate");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="BookNotFoundException", Namespace="http://schemas.datacontract.org/2004/07/BookService.Utils")]
    [System.SerializableAttribute()]
    public partial class BookNotFoundException : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MessageField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message {
            get {
                return this.MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.MessageField, value) != true)) {
                    this.MessageField = value;
                    this.RaisePropertyChanged("Message");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="UserNotFoundException", Namespace="http://schemas.datacontract.org/2004/07/BookService.Utils")]
    [System.SerializableAttribute()]
    public partial class UserNotFoundException : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MessageField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message {
            get {
                return this.MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.MessageField, value) != true)) {
                    this.MessageField = value;
                    this.RaisePropertyChanged("Message");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="BookService.IService1")]
    public interface IService1 {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/ListOfBorrowedItems", ReplyAction="http://tempuri.org/IService1/ListOfBorrowedItemsResponse")]
        BookClient.BookService.Book[] ListOfBorrowedItems();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/ListOfBorrowedItems", ReplyAction="http://tempuri.org/IService1/ListOfBorrowedItemsResponse")]
        System.Threading.Tasks.Task<BookClient.BookService.Book[]> ListOfBorrowedItemsAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/GetBorrowedBooks", ReplyAction="http://tempuri.org/IService1/GetBorrowedBooksResponse")]
        BookClient.BookService.Book[] GetBorrowedBooks(int userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/GetBorrowedBooks", ReplyAction="http://tempuri.org/IService1/GetBorrowedBooksResponse")]
        System.Threading.Tasks.Task<BookClient.BookService.Book[]> GetBorrowedBooksAsync(int userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/GetBookInfo", ReplyAction="http://tempuri.org/IService1/GetBookInfoResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(BookClient.BookService.BookNotFoundException), Action="http://tempuri.org/IService1/GetBookInfoBookNotFoundExceptionFault", Name="BookNotFoundException", Namespace="http://schemas.datacontract.org/2004/07/BookService.Utils")]
        BookClient.BookService.BookInfo GetBookInfo(int bookId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/GetBookInfo", ReplyAction="http://tempuri.org/IService1/GetBookInfoResponse")]
        System.Threading.Tasks.Task<BookClient.BookService.BookInfo> GetBookInfoAsync(int bookId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/BorrowBook", ReplyAction="http://tempuri.org/IService1/BorrowBookResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(BookClient.BookService.BookNotFoundException), Action="http://tempuri.org/IService1/BorrowBookBookNotFoundExceptionFault", Name="BookNotFoundException", Namespace="http://schemas.datacontract.org/2004/07/BookService.Utils")]
        [System.ServiceModel.FaultContractAttribute(typeof(BookClient.BookService.UserNotFoundException), Action="http://tempuri.org/IService1/BorrowBookUserNotFoundExceptionFault", Name="UserNotFoundException", Namespace="http://schemas.datacontract.org/2004/07/BookService.Utils")]
        BookClient.BookService.Status BorrowBook(int bookId, int userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/BorrowBook", ReplyAction="http://tempuri.org/IService1/BorrowBookResponse")]
        System.Threading.Tasks.Task<BookClient.BookService.Status> BorrowBookAsync(int bookId, int userId);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IService1Channel : BookClient.BookService.IService1, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class Service1Client : System.ServiceModel.ClientBase<BookClient.BookService.IService1>, BookClient.BookService.IService1 {
        
        public Service1Client() {
        }
        
        public Service1Client(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public Service1Client(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public Service1Client(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public Service1Client(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public BookClient.BookService.Book[] ListOfBorrowedItems() {
            return base.Channel.ListOfBorrowedItems();
        }
        
        public System.Threading.Tasks.Task<BookClient.BookService.Book[]> ListOfBorrowedItemsAsync() {
            return base.Channel.ListOfBorrowedItemsAsync();
        }
        
        public BookClient.BookService.Book[] GetBorrowedBooks(int userId) {
            return base.Channel.GetBorrowedBooks(userId);
        }
        
        public System.Threading.Tasks.Task<BookClient.BookService.Book[]> GetBorrowedBooksAsync(int userId) {
            return base.Channel.GetBorrowedBooksAsync(userId);
        }
        
        public BookClient.BookService.BookInfo GetBookInfo(int bookId) {
            return base.Channel.GetBookInfo(bookId);
        }
        
        public System.Threading.Tasks.Task<BookClient.BookService.BookInfo> GetBookInfoAsync(int bookId) {
            return base.Channel.GetBookInfoAsync(bookId);
        }
        
        public BookClient.BookService.Status BorrowBook(int bookId, int userId) {
            return base.Channel.BorrowBook(bookId, userId);
        }
        
        public System.Threading.Tasks.Task<BookClient.BookService.Status> BorrowBookAsync(int bookId, int userId) {
            return base.Channel.BorrowBookAsync(bookId, userId);
        }
    }
}
