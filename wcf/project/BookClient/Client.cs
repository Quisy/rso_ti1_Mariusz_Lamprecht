﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using BookClient.BookService;

namespace BookClient
{
    class Client: ClientBase<IService1>, IService1
    {
        public Book[] ListOfBorrowedItems()
        {
            return Channel.ListOfBorrowedItems();
        }

        public Task<Book[]> ListOfBorrowedItemsAsync()
        {
            throw new NotImplementedException();
        }

        public Book[] GetBorrowedBooks(int userId)
        {
            return Channel.GetBorrowedBooks(userId);
        }

        public Task<Book[]> GetBorrowedBooksAsync(int userId)
        {
            throw new NotImplementedException();
        }

        public BookInfo GetBookInfo(int bookId)
        {
            return Channel.GetBookInfo(bookId);
        }

        public Task<BookInfo> GetBookInfoAsync(int bookId)
        {
            throw new NotImplementedException();
        }

        public Status BorrowBook(int bookId, int userId)
        {
            return Channel.BorrowBook(bookId, userId);
        }

        public Task<Status> BorrowBookAsync(int bookId, int userId)
        {
            throw new NotImplementedException();
        }
    }
}
