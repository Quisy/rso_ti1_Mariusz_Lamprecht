﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using BookClient.BookService;

namespace BookClient
{
    class Program
    {
        private static readonly Client _client = new Client();
        private static int _userId = 1;

        static void Main(string[] args)
        {


            while (true)
            {

                Console.Write("MENU: \n" +
                              "1. List of borrwed books\n" +
                              "2. My borrowed books\n" +
                              "3. Book info\n" +
                              "4. Borrow book\n\n");

                var choose = Console.ReadLine();
                switch (choose)
                {
                    case "1":
                        {
                            var books = _client.ListOfBorrowedItems();
                            Console.WriteLine("\n-----------------------------ALL BORROWED BOOKS IDs-----------------------------\n");
                            books.ToList().ForEach(i =>
                                Console.WriteLine($"{i.BookId}")
                            );
                            Console.WriteLine("----------------------------------------------------------------------------------\n\n");


                            break;
                        }

                    case "2":
                        {
                            try
                            {

                                var books = _client.GetBorrowedBooks(_userId);
                                Console.WriteLine("\n-----------------------------MY BORROWED BOOKS IDs-----------------------------\n");
                                books.ToList().ForEach(i =>
                                    Console.WriteLine($"{i.BookId}")
                                );
                                Console.WriteLine("---------------------------------------------------------------------------------\n\n");

                            }
                            catch (FaultException<UserNotFoundException> ex)
                            {
                                Console.WriteLine(ex.Detail.Message);
                            }

                            break;
                        }
                    case "3":
                        {

                            try
                            {
                                int bookId;
                                Console.WriteLine("Book ID: ");
                                int.TryParse(Console.ReadLine(), out bookId);
                                var book = _client.GetBookInfo(bookId);
                                if (book == null)
                                {
                                    Console.WriteLine("\nBook not found\n\n");
                                    break;
                                }

                                Console.Write(
                                    "\n--------------------------\n" +
                                    $"Title: {book.Title}\n" +
                                    $"Author: {book.Author}\n" +
                                    $"Publication Date: {book.PublicationDate}\n" +
                                    "-----------------------------\n\n"
                                    );

                            }
                            catch (FaultException<BookNotFoundException> ex)
                            {
                                Console.WriteLine(ex.Detail.Message);
                            }

                            break;
                        }
                    case "4":
                        {
                            try
                            {
                                int bookId;
                                Console.WriteLine("Book ID: ");
                                int.TryParse(Console.ReadLine(), out bookId);
                                var status = _client.BorrowBook(bookId, _userId);
                                if (status == null)
                                {
                                    Console.WriteLine("\nBook not found\n\n");
                                    break;
                                }

                                Console.Write(
                                    $"\nReturn date: {status.ReturnDate} \n\n "
                                    );

                            }
                            catch (FaultException<BookNotFoundException> ex)
                            {
                                Console.WriteLine(ex.Detail.Message);

                            }
                            catch (FaultException<UserNotFoundException> ex)
                            {
                                Console.WriteLine(ex.Detail.Message);
                            }

                            break;
                        }
                    default:
                        {
                            return;
                        }
                }
            }
        }
    }
}
