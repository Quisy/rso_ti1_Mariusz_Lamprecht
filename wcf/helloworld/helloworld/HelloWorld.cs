﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace helloworld
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class HelloWorld : IHelloWorld
    {
        public double Sqrt(double value)
        {
            return Math.Sqrt(value);
        }
    }
}
