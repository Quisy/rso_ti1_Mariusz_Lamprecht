﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using helloWorldClient.HelloWorldService;

namespace helloWorldClient
{
    public class HelloWorldClient : ClientBase<IHelloWorld>, IHelloWorld
    {
        public double Sqrt(double value)
        {
            return Channel.Sqrt(value);
        }

        public Task<double> SqrtAsync(double value)
        {
            throw new NotImplementedException();
        }
    }
}
