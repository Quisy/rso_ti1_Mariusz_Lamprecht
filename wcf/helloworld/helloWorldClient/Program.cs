﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace helloWorldClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new HelloWorldClient();
            double val;

            while (true)
            {
                Console.WriteLine("Podaj liczbe do spierwiastkowania: ");
                try
                {
                    double.TryParse(Console.ReadLine(), out val);
                    val = client.Sqrt(val);
                    Console.WriteLine("Pierwiastek z liczby: " + val);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Błąd: " + ex.Message);
                }
            }
        }


    }
}
