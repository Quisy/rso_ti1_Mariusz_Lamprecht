#include "Headers/PierwiastekI.h"
#include <math.h>

using namespace std;
using namespace HelloWorld;

double PierwiastekI::pierwiastkuj(const double liczba, const Ice::Current&)
{
    return sqrt(liczba);
}
