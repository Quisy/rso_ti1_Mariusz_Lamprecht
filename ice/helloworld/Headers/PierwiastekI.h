#include <Ice/Ice.h>
#include "Pierwiastek.h"

using namespace std;
using namespace HelloWorld;

class PierwiastekI : public Pierwiastek {
    public:
    virtual double pierwiastkuj(const double liczba, const Ice::Current&);
};